﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassReactsToPlayer : MonoBehaviour
{
    public Material[] materials;
    public Transform male;
    Vector3 theMovement;
   
    
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("the position is " + theMovement);
    }

    void Update()
    {
        theMovement = male.transform.position;
        for (int i=0; i< materials.Length;i++)
        {
            materials[0].SetVector("_position", theMovement);
        }
    }
}
